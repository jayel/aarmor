#ifndef ASCII_ARMOR_H
#define ASCII_ARMOR_H

#ifdef __cplusplus
#   include <cstdint>
#   include <cstring>
#else
#   include <stdint.h>
#   include <string.h>
#endif

#define MAX_LINE_LENGTH 64

#ifdef __cplusplus
extern "C" {
#endif

// Allocates a buffer that will have exactly enough space to contain the
// encoded output of a stream of 'inlen' bytes.
uint8_t* ascii_armor_alloc_for_encoding(size_t inlen, size_t* outlen);

// Allocates a buffer that will have exactly enough space to contain the
// decoded output of a stream of 'inlen' bytes.
uint8_t* ascii_armor_alloc_for_decoding(size_t inlen, size_t* outlen);

// Calculate the CRC-24 of a data stream.
int32_t crc24(const uint8_t* data, size_t len);

// Encode a binary stream into ASCII armor.
// 'src': data to encode.
// 'len': number of bytes to encode.
// 'dest': destination buffer. The function expects a buffer big enough to hold
// all the data.
// Returns the number of bytes copied into 'dest'.
size_t ascii_armor_encode(const uint8_t* src, size_t len, uint8_t* dest);
size_t radix64_encode(const uint8_t* src, size_t len, uint8_t* dest);

// Decode an ASCII armor stream into binary.
// 'src': data to decode.
// 'len': number of bytes to decode.
// 'dest': destination buffer. The function expects a buffer big enough to hold
// all the data.
// Returns the number of bytes copied into 'dest'.
size_t ascii_armor_decode(const uint8_t* src, size_t len, uint8_t* dest);
size_t radix64_decode(const uint8_t* src, size_t len, uint8_t* dest);

#ifdef __cplusplus
}
#endif

#endif
