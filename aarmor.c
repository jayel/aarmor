#include "aarmor.h"

#ifdef __cplusplus
#   include <cstdlib>
#else
#   include <stdlib.h>
#endif

#include <stdio.h>

#define CRC24_INIT 0xB704CEL
#define CRC24_POLY 0x1864CFBL

static uint8_t to_radix64[64] = {
    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
    'G',
    'H',
    'I',
    'J',
    'K',
    'L',
    'M',
    'N',
    'O',
    'P',
    'Q',
    'R',
    'S',
    'T',
    'U',
    'V',
    'W',
    'X',
    'Y',
    'Z',
    'a',
    'b',
    'c',
    'd',
    'e',
    'f',
    'g',
    'h',
    'i',
    'j',
    'k',
    'l',
    'm',
    'n',
    'o',
    'p',
    'q',
    'r',
    's',
    't',
    'u',
    'v',
    'w',
    'x',
    'y',
    'z',
    '0',
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    '+',
    '/'
};

static uint8_t from_radix64[128] = {
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    62,
    0,
    0,
    0,
    63,
    52,
    53,
    54,
    55,
    56,
    57,
    58,
    59,
    60,
    61,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
    25,
    0,
    0,
    0,
    0,
    0,
    0,
    26,
    27,
    28,
    29,
    30,
    31,
    32,
    33,
    34,
    35,
    36,
    37,
    38,
    39,
    40,
    41,
    42,
    43,
    44,
    45,
    46,
    47,
    48,
    49,
    50,
    51,
    0,
    0,
    0,
    0,
};

uint8_t* ascii_armor_alloc_for_encoding(size_t inlen, size_t* outlen)
{
    // The 7 is for the mandatory CRC in its own line
    size_t sz = ((inlen / 3) * 4) + 7;
    sz += inlen % 3? 4 : 0;
    sz += (sz / MAX_LINE_LENGTH) * 2;
    if (outlen)
        *outlen = sz;
    uint8_t* buf = (uint8_t*)malloc(sz);
    memset(buf, 0, sz);
    return buf;
}

uint8_t* ascii_armor_alloc_for_decoding(size_t inlen, size_t* outlen)
{
    size_t sz = (inlen / 4) * 3;
    if (outlen)
        *outlen = sz;
    return (uint8_t*)malloc(sizeof(uint8_t) * sz);
}

int32_t crc24(const uint8_t* octets, size_t len)
{
    int32_t crc = CRC24_INIT;
    int i;
    while (len--) {
        crc ^= (*octets++) << 16;
        for (i = 0; i < 8; i++) {
            crc <<= 1;
            if (crc & 0x1000000)
                crc ^= CRC24_POLY;
        }
    }
    return crc & 0xFFFFFFL;
}

size_t radix64_encode(const uint8_t* src, size_t len, uint8_t* dest)
{
    size_t times = len / 3;
    size_t i = 0;
    size_t count = 0;
    for (; count < times; ++count, i += 3) {
        *dest++ = to_radix64[(src[i] & 0xfc) >> 2];
        *dest++ = to_radix64[((src[i] & 0x03) << 4) | ((src[i + 1] & 0xf0) >> 4)];
        *dest++ = to_radix64[((src[i + 1] & 0x0f) << 2) | ((src[i + 2] & 0xc0) >> 6)];
        *dest++ = to_radix64[src[i + 2] & 0x3f];
        if (((count + 1) % 16) == 0) {
            *dest ++ = '\r';
            *dest ++ = '\n';
        }
    }
    int32_t remainder = len % 3;
    if (remainder == 2) {
        *dest++ = to_radix64[(src[i] & 0xfc) >> 2];
        *dest++ = to_radix64[((src[i] & 0x03) << 4) | ((src[i + 1] & 0xf0) >> 4)];
        *dest++ = to_radix64[((src[i + 1] & 0x0f) << 2) | 0];
        *dest++ = '=';
        ++count;
    }
    else if (remainder == 1) {
        *dest++ = to_radix64[(src[i] & 0xfc) >> 2];
        *dest++ = to_radix64[((src[i] & 0x03) << 4) | 0];
        *dest++ = '=';
        *dest++ = '=';
        ++count;
    }
    if (((count + 1) % 16) == 0) {
        *dest ++ = '\r';
        *dest ++ = '\n';
    }
    return (len / 3) * 4 + (len % 3? 1 : 0);
}

size_t ascii_armor_encode(const uint8_t* src, size_t len, uint8_t* dest)
{
    size_t encoded = radix64_encode(src, len, dest);
    if (dest[encoded - 1] != '\n') {
        dest[encoded++] = '\r';
        dest[encoded++] = '\n';
    }
    dest[encoded++] = '=';
    int32_t crc = crc24(src, len);
    uint8_t* tmp = &crc;
    encoded += radix64_encode(tmp + 1, 3, dest + encoded);
    return encoded;
}

size_t radix64_decode(const uint8_t* src, size_t len, uint8_t* dest)
{
    size_t times = len / 4;
    size_t i = 0;
    size_t copied = 0;
    for (size_t count = 0; count < (times - 1); ++count, i += 4, copied += 3) {
        *dest++ = (from_radix64[src[i]] << 2) | ((from_radix64[src[i + 1]] & 0x30) >> 4);
        *dest++ = ((from_radix64[src[i + 1]] & 0x0f) << 4) | ((from_radix64[src[i + 2]] & 0x3c) >> 2);
        *dest++ = ((from_radix64[src[i + 2]] & 0x03) << 6) | ((from_radix64[src[i + 3]] & 0x3f));
    }
    *dest++ = ((from_radix64[src[i]] << 2) | (from_radix64[src[i + 1]] & 0x30) >> 4);
    ++copied;
    if (src[i + 2] != '=') {
        *dest++ = (((from_radix64[src[i + 1]] & 0x0f) << 4) | (from_radix64[src[i + 2]] & 0x3c) >> 2);
        ++copied;
    }
    if (src[i + 3] != '=') {
        *dest++ = (((from_radix64[src[i + 2]] & 0x03) << 6) | (from_radix64[src[i + 3]] & 0x3f));
        ++copied;
    }
    return copied;
}

size_t ascii_armor_decode(const uint8_t* src, size_t len, uint8_t* dest)
{
    return radix64_decode(src, len, dest);
}

