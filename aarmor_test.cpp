#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE aarmor
#include <boost/test/unit_test.hpp>
#include <vector>
#include <iostream>
#include "aarmor.h"

using namespace std;

void dump(uint8_t* buf, size_t len)
{
    for (size_t i = 0; i < len; ++i) {
        cout << std::hex << (int)buf[i] << ", ";
    }
    cout << "\n";
}

void dump_char(uint8_t* buf, size_t len)
{
    for (size_t i = 0; i < len; ++i) {
        cout << buf[i] << ", ";
    }
    cout << "\n";
}

BOOST_AUTO_TEST_CASE(radix64_encode_full)
{
    vector<uint8_t> data = {0x14, 0xfb, 0x9c, 0x3, 0xd9, 0x7e};
    size_t armored_size = 0;
    size_t reversed_size = 0;
    uint8_t* armored = ascii_armor_alloc_for_encoding(data.size(), &armored_size);
    size_t copied = radix64_encode(data.data(), data.size(), armored);
    uint8_t* reversed = ascii_armor_alloc_for_decoding(copied, &reversed_size);
    copied = radix64_decode(armored, copied, reversed);
    BOOST_CHECK_EQUAL(equal(reversed, reversed + copied, data.cbegin()), true);
    free(armored);
    free(reversed);
}

BOOST_AUTO_TEST_CASE(radix64_encode_one_pad)
{
    vector<uint8_t> data = {0x14, 0xfb, 0x9c, 0x3, 0xd9};
    size_t armored_size = 0;
    size_t reversed_size = 0;
    uint8_t* armored = ascii_armor_alloc_for_encoding(data.size(), &armored_size);
    size_t copied = radix64_encode(data.data(), data.size(), armored);
    uint8_t* reversed = ascii_armor_alloc_for_decoding(copied, &reversed_size);
    copied = radix64_decode(armored, copied, reversed);
    BOOST_CHECK_EQUAL(equal(reversed, reversed + copied, data.cbegin()), true);
    free(armored);
    free(reversed);
}

BOOST_AUTO_TEST_CASE(radix64_encode_two_pad)
{
    vector<uint8_t> data = {0x14, 0xfb, 0x9c, 0x3};
    size_t armored_size = 0;
    size_t reversed_size = 0;
    uint8_t* armored = ascii_armor_alloc_for_encoding(data.size(), &armored_size);
    size_t copied = radix64_encode(data.data(), data.size(), armored);
    uint8_t* reversed = ascii_armor_alloc_for_decoding(copied, &reversed_size);
    copied = radix64_decode(armored, copied, reversed);
    BOOST_CHECK_EQUAL(equal(reversed, reversed + copied, data.cbegin()), true);
    free(armored);
    free(reversed);
}

BOOST_AUTO_TEST_CASE(ascii_armor_enc)
{
    vector<uint8_t> data = {0x14, 0xfb, 0x9c, 0x3, 0xd9, 0x7e};
    size_t armored_size = 0;
    uint8_t* armored = ascii_armor_alloc_for_encoding(data.size(), &armored_size);
    size_t copied = ascii_armor_encode(data.data(), data.size(), armored);
    cout << "Armored: " << (char*)armored << "\n";
    free(armored);
}
